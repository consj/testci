#INFOFILE1.1 - Do not remove this line!
FileIdent = CarMaker-TestSeries 2
FileCreator = CarMaker 5.1 2016-2-29
Description:
LastChange = 2016-08-15 17:17:45 NZ1404
StartTime = 2016-08-15 15:52:43
EndTime = 2016-08-15 15:53:01
ReportTemplate =
Step.0 = Settings
Step.0.Name = Global Settings
Step.1 = TestRun
Step.1.Name = steer_while_braking
Step.1.Param.0 = m_acc NValue
Step.1.Param.1 = yaw_min NValue
Step.1.Param.2 = yaw_max NValue
Step.1.Param.3 = max_side_sleep NValue
Step.1.Char.0.Name = one sec interval
Step.1.Char.0.Description:
Step.1.Char.0.Identifier = one_sec_interval
Step.1.Char.0.Unit =
Step.1.Char.0.Param.0 = RTexpr {first() ? Qu::one_sec_interval = 0}
Step.1.Char.0.Param.1 = RTexpr {first(DM.ManNo==1) ? one_sec_interval=1}
Step.1.Char.0.Param.2 = RTexpr {first(DM.ManNo==2 & DM.ManTime >= 0.9 & DM.ManTime <=1.1) ? one_sec_interval=2}
Step.1.Char.0.Param.3 = RTexpr {first(DM.ManNo==2 & DM.ManTime >= 0.9 & DM.ManTime >=1.1) ? one_sec_interval=3}
Step.1.Char.1.Name = Characteristic 1
Step.1.Char.1.Description:
Step.1.Char.1.Identifier = re_yaw_rate
Step.1.Char.1.Unit =
Step.1.Crit.0.Name = Yaw Rate Ratio
Step.1.Crit.0.Description:
Step.1.Crit.0.Good = [get ratio ] > $TS::ratio_limit && [get ratio ] < 1.5
Step.1.Crit.0.Warn =
Step.1.Crit.0.Bad =
Step.1.Crit.1.Name = Peak Side Slip
Step.1.Crit.1.Description:
Step.1.Crit.1.Good = [get peak_side_slip] <10 && [get peak_side_slip]>-10
Step.1.Crit.1.Warn =
Step.1.Crit.1.Bad =
Step.1.Var.0.Name = Variation 0
Step.1.Var.0.Param = 2.5 0.83 1.5 10
Step.1.Var.0.Result = good
Step.1.Var.0.ResDate = 1471269181
Step.1.Var.0.ResFiles = SimOutput/SENEVSCL0458/20160815/steer_while_braking_155247.erg
Step.1.Var.0.ManLst = 0 1 2
Step.1.Var.1.Name = Variation 2
Step.1.Var.1.Param = 5 0.66 1.5 10
Step.1.Var.1.Result = good
Step.1.Var.1.ResDate = 1471269096
Step.1.Var.1.ResFiles = SimOutput/SENEVSCL0458/20160815/steer_while_braking_155123.erg
Step.1.Var.1.ManLst = 0 1 2
Step.1.Var.2.Name = Variation 3
Step.1.Var.2.Param = 7.5 0.5 1.5 10
Step.1.Var.2.Result = good
Step.1.Var.2.ResDate = 1471269114
Step.1.Var.2.ResFiles = SimOutput/SENEVSCL0458/20160815/steer_while_braking_155140.erg
Step.1.Var.2.ManLst = 0 1 2
Step.1.Var.3.Name = Variation 4
Step.1.Var.3.Param = 10 0.33 1.5 10
Step.1.Var.3.Result = good
Step.1.Var.3.ResDate = 1471269132
Step.1.Var.3.ResFiles = SimOutput/SENEVSCL0458/20160815/steer_while_braking_155158.erg
Step.1.Var.3.ManLst = 0 1 2
Step.1.Var.4.Name = Variation 5
Step.1.Var.4.Param = 30 0 20 50
Step.1.Var.4.Result = good
Step.1.Var.4.ResDate = 1471269151
Step.1.Var.4.ResFiles = SimOutput/SENEVSCL0458/20160815/steer_while_braking_155217.erg
Step.1.Var.4.ManLst = 0 1 2
