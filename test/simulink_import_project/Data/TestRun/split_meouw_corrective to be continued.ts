#INFOFILE1.1 - Do not remove this line!
FileIdent = CarMaker-TestSeries 2
FileCreator = CarMaker 5.1.1 2016-5-4
Description:
LastChange = 2016-08-23 14:44:07 NZ1404
StartTime = 2016-08-16 15:52:11
EndTime = 2016-08-16 15:52:30
ReportTemplate =
Step.0 = Settings
Step.0.Name = Global Settings
Step.1 = TestRun
Step.1.Name = Braking_musplit
Step.1.Param.0 = left_stripe NValue
Step.1.Param.1 = right_stripe NValue
Step.1.Param.2 = yaw_rate_crit TS
Step.1.Param.3 = yaw_acc_crit TS
Step.1.Param.4 = steering_limit NValue
Step.1.Char.0.Name = One sec Interval
Step.1.Char.0.Description:
Step.1.Char.0.Identifier = one_sec_interval
Step.1.Char.0.Unit =
Step.1.Char.0.Param.0 = RTexpr {first() ? Qu::one_sec_interval = 0}
Step.1.Char.0.Param.1 = RTexpr {first(DM.ManNo==1) ? one_sec_interval=1}
Step.1.Char.0.Param.2 = RTexpr {first(DM.ManNo==2 & DM.ManTime >= 0.9 & DM.ManTime <=1.1) ? one_sec_interval=2}
Step.1.Char.1.Name = Average yaw rate
Step.1.Char.1.Description:
Step.1.Char.1.Identifier = aveg_yaw_rate
Step.1.Char.1.Unit =
Step.1.Char.1.Param.0 = RTexpr {first()? Qu::aveg_yaw_rate=0}
Step.1.Char.1.Param.1 = RTexpr {aveg_yaw_rate= mean(Vhcl.YawRate, one_sec_interval == 2)}
Step.1.Char.2.Name = Average Yaw accel
Step.1.Char.2.Description:
Step.1.Char.2.Identifier = ratio
Step.1.Char.2.Unit =
Step.1.Char.2.Param.0 = RTexpr {first()? Qu::ratio=0}
Step.1.Char.2.Param.1 = RTexpr {aveg_yaw_rate= mean(Vhcl.YawRate, one_sec_interval == 2)}
Step.1.Crit.0.Name = Criterion 0
Step.1.Crit.0.Description:
Step.1.Crit.0.Good =
Step.1.Crit.0.Warn =
Step.1.Crit.0.Bad =
Step.1.Var.0.Name = Slip_m_no_steering
Step.1.Var.0.Param = 1 0.4 15 3 1.1
Step.1.Var.0.Result = good
Step.1.Var.0.ResDate = 1471355550
Step.1.Var.0.ResFiles = SimOutput/SENEVSCL0458/20160816/Braking_musplit_155218.erg
Step.1.Var.0.ManLst = 0 1 2
Step.1.Var.0.Char.0.Name = one_sec_interval
Step.1.Var.0.Char.0.Value = 2.0
Step.1.Var.0.Char.1.Name = aveg_yaw_rate
Step.1.Var.0.Char.1.Value = 0.5754128694534302
Step.1.Var.0.Char.2.Name = ratio
Step.1.Var.0.Char.2.Value = 0.0
Step.1.Var.1.Name = Slip_m_yes_steering
Step.1.Var.1.Param = 1 0.4 10 3 90
Step.1.Var.1.Result = good
Step.1.Var.1.ResDate = 1471355516
Step.1.Var.1.ResFiles = SimOutput/SENEVSCL0458/20160816/Braking_musplit_155145.erg
Step.1.Var.1.ManLst = 0 1 2
Step.1.Var.1.Char.0.Name = one_sec_interval
Step.1.Var.1.Char.0.Value = 2.0
Step.1.Var.1.Char.1.Name = aveg_yaw_rate
Step.1.Var.1.Char.1.Value = 0.6000524759292603
Step.1.Var.1.Char.2.Name = ratio
Step.1.Var.1.Char.2.Value = 0.0
Step.1.Var.2.Name = Slip_m_no_steering
Step.1.Var.2.Param = 0.4 1 15 3 1.1
Step.1.Var.2.Result = good
Step.1.Var.2.ResDate = 1471354381
Step.1.Var.2.ResFiles = SimOutput/SENEVSCL0458/20160816/Braking_musplit_153249.erg
Step.1.Var.2.ManLst = 0 1 2
Step.1.Var.2.Char.0.Name = one_sec_interval
Step.1.Var.2.Char.0.Value = 2.0
Step.1.Var.2.Char.1.Name = aveg_yaw_rate
Step.1.Var.2.Char.1.Value = -0.5754123330116272
Step.1.Var.2.Char.2.Name = ratio
Step.1.Var.2.Char.2.Value = 0.0
Step.1.Var.3.Name = Slip_m_yes_steering
Step.1.Var.3.Param = 0.4 1 10 3 90
Step.1.Var.3.Result = good
Step.1.Var.3.ResDate = 1471354399
Step.1.Var.3.ResFiles = SimOutput/SENEVSCL0458/20160816/Braking_musplit_153307.erg
Step.1.Var.3.ManLst = 0 1 2
Step.1.Var.3.Char.0.Name = one_sec_interval
Step.1.Var.3.Char.0.Value = 2.0
Step.1.Var.3.Char.1.Name = aveg_yaw_rate
Step.1.Var.3.Char.1.Value = -0.600055992603302
Step.1.Var.3.Char.2.Name = ratio
Step.1.Var.3.Char.2.Value = 0.0
