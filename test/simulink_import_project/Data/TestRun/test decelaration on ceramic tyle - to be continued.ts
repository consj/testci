#INFOFILE1.1 - Do not remove this line!
FileIdent = CarMaker-TestSeries 2
FileCreator = CarMaker 5.1.2 2016-8-12
Description:
LastChange = 2016-09-07 12:23:52 NZ1404
StartTime = 2016-09-07 11:01:53
EndTime = 2016-09-07 11:02:15
ReportTemplate =
Step.0 = Settings
Step.0.Name = Global Settings
Step.1 = TestRun
Step.1.Name = Deceleration on Split Coefficient of Friction Surfaces
Step.1.Param.0 = friction NValue
Step.1.Param.1 = speed NValue
Step.1.Char.0.Name = Deceleration ratio
Step.1.Char.0.Description:
Step.1.Char.0.Identifier = Characteristic_0
Step.1.Char.0.Unit =
Step.1.Crit.0.Name = Minimum Sustained Deceleration Ratio
Step.1.Crit.0.Description:
Step.1.Crit.0.Good =
Step.1.Crit.0.Warn =
Step.1.Crit.0.Bad =
Step.1.Var.0.Name = Deceleration on Dry Asphalt/Wet Ceramic Tile
Step.1.Var.0.Param = 0.2 70
Step.1.Var.0.Result = good
Step.1.Var.0.ResDate = 1473238919
Step.1.Var.0.ResFiles = {SimOutput/SENEVSCL0458/20160907/Deceleration on Split Coefficient of Friction Surfaces_110155.erg}
Step.1.Var.0.ManLst = 0 1 2 3
Step.1.Var.1.Name = Deceleration on Dry Asphalt/Wet Basalt
Step.1.Var.1.Param = 0.3 70
Step.1.Var.1.Result = good
Step.1.Var.1.ResDate = 1473238924
Step.1.Var.1.ResFiles = {SimOutput/SENEVSCL0458/20160907/Deceleration on Split Coefficient of Friction Surfaces_110201.erg}
Step.1.Var.1.ManLst = 0 1 2
Step.1.Var.2.Name = Deceleration on Dry Asphalt/Ice
Step.1.Var.2.Param = 0.1 70
Step.1.Var.2.Result = good
Step.1.Var.2.ResDate = 1473238930
Step.1.Var.2.ResFiles = {SimOutput/SENEVSCL0458/20160907/Deceleration on Split Coefficient of Friction Surfaces_110206.erg}
Step.1.Var.2.ManLst = 0 1 2 3
Step.1.Var.3.Name = Deceleration on Dry Asphalt/Gravel
Step.1.Var.3.Param = 0.5 100
Step.1.Var.3.Result = good
Step.1.Var.3.ResDate = 1473238935
Step.1.Var.3.ResFiles = {SimOutput/SENEVSCL0458/20160907/Deceleration on Split Coefficient of Friction Surfaces_110211.erg}
Step.1.Var.3.ManLst = 0 1 2 3
