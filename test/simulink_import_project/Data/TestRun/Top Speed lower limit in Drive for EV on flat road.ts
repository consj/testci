#INFOFILE1.1 - Do not remove this line!
FileIdent = CarMaker-TestSeries 2
FileCreator = CarMaker 5.1.1 2016-5-4
Description:
LastChange = 2016-09-02 08:55:28 NZ1404
StartTime = 2016-09-02 08:49:31
EndTime = 2016-09-02 08:52:02
ReportTemplate =
Step.0 = Settings
Step.0.Name = Global Settings
Step.1 = TestRun
Step.1.Name = Top Speed lower limit in Drive for EV on flat road
Step.1.Param.0 = speed_limit NValue
Step.1.Char.0.Name = Speed limit trigger
Step.1.Char.0.Description:
Step.1.Char.0.Identifier = low_speed_trigger
Step.1.Char.0.Unit =
Step.1.Char.0.Param.0 = RTexpr {first() ? Qu::low_speed_trigger=0}
Step.1.Char.0.Param.1 = RTexpr {first(DM.ManNo==2 && Car.v <= $speed_limit) ? low_speed_trigger=1}
Step.1.Crit.0.Name = Low Speed Limit
Step.1.Crit.0.Description:
Step.1.Crit.0.Good = [get low_speed_trigger] == 0
Step.1.Crit.0.Warn =
Step.1.Crit.0.Bad = [get low_speed_trigger] == 1
Step.1.Diag.0.Name = Diagram 0
Step.1.Diag.0.Type = Line Diagram
Step.1.Diag.0.Mode = Quantity vs Time
Step.1.Diag.0.VarOpt = 0
Step.1.Diag.0.Grid = Both
Step.1.Diag.0.NAxes = 1
Step.1.Diag.0.RefFile =
Step.1.Diag.0.Pic =
Step.1.Diag.0.Param1.0 = Auto {} {} {}
Step.1.Diag.0.Param1.1 = Auto {} {} {}
Step.1.Diag.0.Param2.0 = Time {} {} {}
Step.1.Diag.0.Param2.1 = Car.vx {} {} {}
Step.1.Diag.0.Param2.2 = {} {} {} {}
Step.1.Diag.0.Param3.0 = {} {} {} {}
Step.1.Diag.0.Param3.1 = {} {} {} {}
Step.1.Diag.0.Param3.2 = {} {} {} {}
Step.1.Var.0.Name = Req. 870 - Min. 30 min speed
Step.1.Var.0.Param = 140/3.6
Step.1.Var.0.Result = bad
Step.1.Var.0.ResDate = 1472799122
Step.1.Var.0.ResFiles = SimOutput/SENEVSCL0458/20160902/Top Speed lower limit in Drive for EV on flat road_084931.erg
Step.1.Var.0.ManLst = 0 1 2
Step.1.Var.0.Char.0.Name = low_speed_trigger
Step.1.Var.0.Char.0.Value = 1.0
Step.1.Var.0.Crit.0.Name = Low Speed Limit
Step.1.Var.0.Crit.0.Result = bad
Step.1.Var.0.Diag.0.Pic = error
