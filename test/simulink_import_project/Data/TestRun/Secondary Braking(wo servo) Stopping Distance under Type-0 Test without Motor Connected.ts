#INFOFILE1.1 - Do not remove this line!
FileIdent = CarMaker-TestSeries 2
FileCreator = CarMaker 5.1.1 2016-5-4
Description:
LastChange = 2016-09-07 09:22:47 KZ8224
StartTime = 2016-09-07 09:20:53
EndTime = 2016-09-07 09:21:04
ReportTemplate =
Step.0 = Settings
Step.0.Name = Global Settings
Step.1 = TestRun
Step.1.Name = Secondary Braking(wo servo) Stopping Distance under Type-0 Test without Motor Connected
Step.1.Char.0.Name = stopping_dist
Step.1.Char.0.Description:
Step.1.Char.0.Identifier = stopping_dist
Step.1.Char.0.Unit = m
Step.1.Char.0.Param.0 = RTexpr {(first () ? Qu::stopping_dist=0); DM.ManNo>0 ? stopping_dist=Delta2Ev(Car.Road.sRoad, (DM.ManNo > 0), Car.v <=0.1)}
Step.1.Char.0.Param.1 = EndProc {Log [get stopping_dist]}
Step.1.Char.1.Name = max_yaw_ang
Step.1.Char.1.Description:
Step.1.Char.1.Identifier = max_yaw_ang
Step.1.Char.1.Unit = rad
Step.1.Char.1.Param.0 = RTexpr {(first() ? Qu::max_yaw_ang = 0); max_yaw_ang = max(max_yaw_ang,Car.Yaw);}
Step.1.Char.2.Name = wheel_lock
Step.1.Char.2.Description:
Step.1.Char.2.Identifier = wheel_lock
Step.1.Char.2.Unit =
Step.1.Char.2.Param.0 = RTexpr {(first() ? Qu::wheel_lock= 1); Car.v > (15/3.6) ? wheel_lock = Car.WheelSpd_FL}
Step.1.Crit.0.Name = Stopping distance
Step.1.Crit.0.Description:
	d < 168 m
Step.1.Crit.0.Good = [get stopping_dist] < 168
Step.1.Crit.0.Warn =
Step.1.Crit.0.Bad = [get stopping_dist] >= 168
Step.1.Crit.1.Name = Common Requirement: No Wheel Locking
Step.1.Crit.1.Description:
	The prescribed performance shall be obtained without locking of the wheels at speeds exceeding 15 km/h.
	""
	This requirement applies for all the stopping distance relevant tests.
Step.1.Crit.1.Good = [get wheel_lock] >= 0.01
Step.1.Crit.1.Warn =
Step.1.Crit.1.Bad = [get wheel_lock] < 0.01
Step.1.Crit.2.Name = Common Requirement: Yaw Angle Limit
Step.1.Crit.2.Description:
	The prescribed performance shall be obtained without exceeding a yaw angle of 15°
	""
	This requirement applies for all the stopping distance relevant tests.
Step.1.Crit.2.Good =
Step.1.Crit.2.Warn =
Step.1.Crit.2.Bad = [get max_yaw_ang] > 0.2618
Step.1.Crit.3.Name = Common Requirement: Lower Limit of the Force Applied to the Service Brake Control
Step.1.Crit.3.Description:
	The force applied to the service brake control shall not be less than 65 N
	""
	This requirement applies for all the stopping distance relevant tests.
Step.1.Crit.3.Good =
Step.1.Crit.3.Warn =
Step.1.Crit.3.Bad =
Step.1.Crit.4.Name = Common Requirement: Upper Limit of the Force Applied to the Service Brake Control
Step.1.Crit.4.Description:
	The force applied to the service brake control shall not exceeding 500 N
	""
	This requirement applies for all the stopping distance relevant tests.
Step.1.Crit.4.Good =
Step.1.Crit.4.Warn =
Step.1.Crit.4.Bad =
Step.1.Diag.0.Name = Diagram 0
Step.1.Diag.0.Type = Line Diagram
Step.1.Diag.0.Mode = Quantity vs Time
Step.1.Diag.0.VarOpt = 0
Step.1.Diag.0.Grid = None
Step.1.Diag.0.NAxes = 1
Step.1.Diag.0.RefFile =
Step.1.Diag.0.Pic =
Step.1.Diag.0.Param1.0 = Auto {} {} {}
Step.1.Diag.0.Param1.1 = Auto {} {} {}
Step.1.Diag.0.Param2.0 = Time {} {} {}
Step.1.Diag.0.Param2.1 = stopping_dist {} {} {}
Step.1.Diag.0.Param2.2 = {} {} {} {}
Step.1.Diag.0.Param3.0 = {} {} {} {}
Step.1.Diag.0.Param3.1 = {} {} {} {}
Step.1.Diag.0.Param3.2 = {} {} {} {}
Step.1.Diag.1.Name = Diagram 0
Step.1.Diag.1.Type = Line Diagram
Step.1.Diag.1.Mode = Quantity vs Time
Step.1.Diag.1.VarOpt = 0
Step.1.Diag.1.Grid = None
Step.1.Diag.1.NAxes = 1
Step.1.Diag.1.RefFile =
Step.1.Diag.1.Pic =
Step.1.Diag.1.Param1.0 = Auto {} {} {}
Step.1.Diag.1.Param1.1 = Auto {} {} {}
Step.1.Diag.1.Param2.0 = Time {} {} {}
Step.1.Diag.1.Param2.1 = max_yaw_ang {} {} {}
Step.1.Diag.1.Param2.2 = {} {} {} {}
Step.1.Diag.1.Param3.0 = {} {} {} {}
Step.1.Diag.1.Param3.1 = {} {} {} {}
Step.1.Diag.1.Param3.2 = {} {} {} {}
Step.1.Diag.2.Name = Diagram 0
Step.1.Diag.2.Type = Line Diagram
Step.1.Diag.2.Mode = Quantity vs Time
Step.1.Diag.2.VarOpt = 0
Step.1.Diag.2.Grid = None
Step.1.Diag.2.NAxes = 1
Step.1.Diag.2.RefFile =
Step.1.Diag.2.Pic =
Step.1.Diag.2.Param1.0 = Auto {} {} {}
Step.1.Diag.2.Param1.1 = Auto {} {} {}
Step.1.Diag.2.Param2.0 = Time {} {} {}
Step.1.Diag.2.Param2.1 = wheel_lock {} {} {}
Step.1.Diag.2.Param2.2 = {} {} {} {}
Step.1.Diag.2.Param3.0 = {} {} {} {}
Step.1.Diag.2.Param3.1 = {} {} {} {}
Step.1.Diag.2.Param3.2 = {} {} {} {}
Step.1.Char.0.Value = NaN
Step.1.Char.1.Value = NaN
Step.1.Char.2.Value = NaN
