#INFOFILE1.1 - Do not remove this line!
FileIdent = CarMaker-TestSeries 2
FileCreator = CarMaker 5.1.2 2016-8-12
Description:
LastChange = 2016-09-14 10:40:24 KZ8224
StartTime = 2016-09-14 10:00:06
EndTime = 2016-09-14 10:00:10
ReportTemplate =
Step.0 = Settings
Step.0.Name = Global Settings
Step.1 = TestRun
Step.1.Name = Hill Start Capability Forward 12% Grade
Step.1.Char.0.Name = elapsed_time
Step.1.Char.0.Description:
Step.1.Char.0.Identifier = elapsed_time
Step.1.Char.0.Unit =
Step.1.Char.0.Param.0 = RTexpr {(first () ? Qu::elapsed_time=0); DM.ManNo>0 ? elapsed_time=Delta2Ev(time, (DM.ManNo > 0), Car.v > 2)}
Step.1.Char.0.Param.1 = EndProc {Log [get elapsed_time]}
Step.1.Crit.0.Name = Criterion 0
Step.1.Crit.0.Description:
Step.1.Crit.0.Good = [get elapsed_time] < 12
Step.1.Crit.0.Warn =
Step.1.Crit.0.Bad =
Step.1.Result = good
Step.1.ResDate = 1473840010
Step.1.ManLst = 0
Step.1.Char.0.Value = 0.0
Step.1.Crit.0.Result = good
