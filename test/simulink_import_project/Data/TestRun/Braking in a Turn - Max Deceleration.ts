#INFOFILE1.1 - Do not remove this line!
FileIdent = CarMaker-TestSeries 2
FileCreator = CarMaker 5.1 2016-2-29
Description:
LastChange = 2016-08-10 09:43:48 NZ1404
StartTime = 2016-08-10 09:40:43
EndTime = 2016-08-10 09:42:52
ReportTemplate =
Step.0 = Settings
Step.0.Name = Global Settings
Step.1 = TestRun
Step.1.Name = Braking_TEST
Step.1.Param.0 = decelaration NValue
Step.1.Param.1 = ratio_limit TS
Step.1.Char.0.Name = One sec interval
Step.1.Char.0.Description:
Step.1.Char.0.Identifier = one_sec_interval
Step.1.Char.0.Unit =
Step.1.Char.0.Param.0 = RTexpr {first() ? Qu::one_sec_interval = 0}
Step.1.Char.0.Param.1 = RTexpr {first(DM.ManNo==1) ? one_sec_interval=1}
Step.1.Char.0.Param.2 = RTexpr {first(DM.ManNo==2 & DM.ManTime >= 0.9 & DM.ManTime <=1.1) ? one_sec_interval=2}
Step.1.Char.0.Param.3 = RTexpr {first(DM.ManNo==2 & DM.ManTime >= 0.9 & DM.ManTime >=1.1) ? one_sec_interval=3}
Step.1.Char.1.Name = Reference yaw rate
Step.1.Char.1.Description:
Step.1.Char.1.Identifier = ref_yaw_rate
Step.1.Char.1.Unit =
Step.1.Char.1.Param.0 = RTexpr {first()? Qu::ref_yaw_rate=0}
Step.1.Char.1.Param.1 = RTexpr {ref_yaw_rate= mean(Vhcl.YawRate, DM.ManNo==1)}
Step.1.Char.2.Name = Average yaw rate
Step.1.Char.2.Description:
Step.1.Char.2.Identifier = aveg_yaw_rate
Step.1.Char.2.Unit =
Step.1.Char.2.Param.0 = RTexpr {first()? Qu::aveg_yaw_rate=0}
Step.1.Char.2.Param.1 = RTexpr {aveg_yaw_rate= mean(Vhcl.YawRate, one_sec_interval == 2)}
Step.1.Char.3.Name = Ratio
Step.1.Char.3.Description:
Step.1.Char.3.Identifier = ratio
Step.1.Char.3.Unit =
Step.1.Char.3.Param.0 = RTexpr {first()? Qu::ratio=0}
Step.1.Char.3.Param.1 = RTexpr {ratio = aveg_yaw_rate/ref_yaw_rate}
Step.1.Crit.0.Name = Yaw rate ratio
Step.1.Crit.0.Description:
Step.1.Crit.0.Good = [get ratio ]  > $TS::ratio_limit  && [get ratio ] <  1.5
Step.1.Crit.0.Warn =
Step.1.Crit.0.Bad =
Step.1.Var.0.Name = Variation 9
Step.1.Var.0.Param = -30 0.3
Step.1.Var.0.Result = good
Step.1.Var.0.ResDate = 1470814972
Step.1.Var.0.ResFiles = SimOutput/SENEVSCL0458/20160810/Braking_TEST_094241.erg
Step.1.Var.0.ManLst = 0 1 2
Step.1.Var.0.Char.0.Name = one_sec_interval
Step.1.Var.0.Char.0.Value = 3.0
Step.1.Var.0.Char.1.Name = ref_yaw_rate
Step.1.Var.0.Char.1.Value = -0.22548826038837433
Step.1.Var.0.Char.2.Name = aveg_yaw_rate
Step.1.Var.0.Char.2.Value = -0.09753584861755371
Step.1.Var.0.Char.3.Name = ratio
Step.1.Var.0.Char.3.Value = 0.4325539767742157
Step.1.Var.0.Crit.0.Name = Yaw rate ratio
Step.1.Var.0.Crit.0.Result = good
TS.ratio_limit = 0.3