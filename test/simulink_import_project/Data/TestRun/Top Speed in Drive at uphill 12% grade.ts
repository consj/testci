#INFOFILE1.1 - Do not remove this line!
FileIdent = CarMaker-TestSeries 2
FileCreator = CarMaker 5.1.2 2016-8-12
Description:
LastChange = 2016-09-07 15:07:56 NZ1404
StartTime = 2016-09-07 15:02:47
EndTime = 2016-09-07 15:02:54
ReportTemplate =
Step.0 = Settings
Step.0.Name = Global Settings
Step.1 = TestRun
Step.1.Name = Top Speed in Drive at uphill
Step.1.Param.0 = grad NValue
Step.1.Param.1 = speed_estimated NValue
Step.1.Param.2 = speed_limit NValue
Step.1.Char.0.Name = Low speed trigger calculation
Step.1.Char.0.Description:
Step.1.Char.0.Identifier = speed_trigger
Step.1.Char.0.Unit =
Step.1.Char.0.Param.0 = RTexpr {first() ? Qu::speed_trigger=0}
Step.1.Char.0.Param.1 = RTexpr {first(DM.ManNo==1 && Car.v <= $speed_limit) ? speed_trigger=1}
Step.1.Crit.0.Name = Lower Speed Limit
Step.1.Crit.0.Description:
Step.1.Crit.0.Good = [get speed_trigger] == 0
Step.1.Crit.0.Warn =
Step.1.Crit.0.Bad =
Step.1.Var.0.Name = Top Speed in Drive at uphill 12% grade
Step.1.Var.0.Param = 12 85 80/3.6
Step.1.Var.0.Result = good
Step.1.Var.0.ResDate = 1473253374
Step.1.Var.0.ResFiles = {SimOutput/SENEVSCL0458/20160907/Top Speed in Drive at uphill_150248.erg}
Step.1.Var.0.ManLst = 0 1
Step.1.Var.0.Char.0.Name = speed_trigger
Step.1.Var.0.Char.0.Value = 0.0
Step.1.Var.0.Crit.0.Name = Lower Speed Limit
Step.1.Var.0.Crit.0.Result = good
